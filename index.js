const express = require ('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.json);
app.use(bodyParser.urlencoded({extended:true}));
const port = 8000;

//habilitar servidor
app.listen(port,()=>{
       console.log("Projeto executando na porta  x: " + port);
});

//recurso de consulta
app.get('/funcionario',(req, res)=>{
     console.log("Acessando o recurso funcionario");
     console.log(req.query.nome);
     res.send("{message:/get funcionario}");
});


//criando outro recurso(que tras o nome e sobrenome)
app.get('/pesquisar', (req, res)=>{
     let dados = req.query;
     res.send(dados.nome + " " + dados.sobrenome);
});

//esse recurso, o (:codigo) é recuperado via "param"
app.get('/pesquisa/cliente/:codigo',(req, res)=>{
    let id = req.params.codigo;
    res.send("Dados do cliente " + id);
});

//recurso que envia parametros via "body"
//GET= pesquisar, enquanto o método POST = gravar
//REQ= Require (requisição)
//RES= Response (resposta do servidor)
app.post('/funcionario/gravar', (req, res)=>{
     let valores = req.body;
     console.log("Nome do cliente:" + valores.nome);
     console.log("Sobrenome do cliente: " + valores.sobrenome);
     console.log("Idade do cliente"+ valores.idade);
     res.send("SUCESSO!!!");
});